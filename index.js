const http = require('http')
require('dotenv').config()
const mongoose = require('mongoose')
const express = require('express')
const cors = require('cors')
const Note = require('./model/note')

require('dotenv').config()

const app = express()

app.use(express.json())
app.use(cors())
app.use(requestLogger)
app.use(express.static('build'))

app.get('/',(req,res)=>{
  res.send('<h1>Test endponint</h1>')
})

app.get('/api/notes',(req,res)=>{
  // res.json(notes)
  Note.find({}).then(notes => {
    res.json(notes)
  })
})

app.get('/api/notes/:id',(req,res)=>{
  Note.findOne({
    id
  }).then(note => {
    res.json(note)
  })
  
})


app.delete('/api/notes/:id', (request, response) => {
  //todo
  response.status(204).end()
})

app.post('/api/notes',(req,res)=>{
  const body = req.body

  if (body.content === undefined) {
    return res.status(400).json({ error: 'content missing' })
  }

  const note = new Note({
    content: body.content,
    important: body.important || false,
  })

  note.save().then(savedNote => {
    res.json(savedNote)
  })

  res.json(note)
})
app.put('/api/notes/:id',(req,res)=>{
  const id = req.params.id
  const body = req.body
  console.log(`id ${id}`)
  console.log(`body ${JSON.stringify(body)}`)

  Note.findOneAndUpdate(
      { _id : id },
      {
        $set : {
          important : req.body.important
        }
      },
      { new : true },
    )
    .then((result) => {
      if(result){
        res.json(req.body)
      }else{
        res.sendStatus(404)
      }
    }).catch(error=>{
      console.log('error found',error)
    })
})
function requestLogger(request, response, next){
  console.log('Method:', request.method)
  console.log('Path:  ', request.path)
  console.log('Body:  ', request.body)
  console.log('---')
  next()
}

const PORT = process.env.PORT
app.listen(PORT,() => {
  console.log(`Server running on port ${PORT}`)
})
